package org.acme.resteasy.APIs;

import org.acme.resteasy.database.StudentsDatabase;
import org.acme.resteasy.models.studentModel;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;

import javax.ws.rs.core.*;
import java.util.*;

import static org.mockito.Mockito.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class studentAPIsTest {

    studentAPIs myStudents;
    TestInfo testInfo;
    TestReporter testReporter;

    @BeforeEach
    void init(TestInfo testInfo, TestReporter testReporter){
        this.testInfo = testInfo;
        this.testReporter = testReporter;
        myStudents = mock(studentAPIs.class);

        //Logging Test Data:
        String value = "Running " + this.testInfo.getDisplayName() +
                ", with tags: "+ this.testInfo.getTags() + "\n";
        this.testReporter.publishEntry(value);

    }

    @Test
    @Tag("GET")
    @Order(1)
    @DisplayName("Get All Students")
    void GetAllStudentsTest(){

        when(myStudents.getAllStudents()).thenReturn(
                new LinkedList<>(Arrays.asList(
                        new studentModel("1152003", "Amr Khaled", "2015", "CCE-C"),
                        new studentModel("1152020", "Hussain Yahya", "2015", "CCE-C"),
                        new studentModel("1152021", "Mohamed Kadry", "2015", "CCE-E"),
                        new studentModel("1152022", "Ahmed Hatem", "2016", "CCE-C"),
                        new studentModel("1152023", "Ahmed Anwar", "2016", "CCE-E")
                )));

        List<studentModel> expected = StudentsDatabase.getStudents();
        List<studentModel> actual = myStudents.getAllStudents();

        Assertions.assertEquals (Arrays.toString(new List[]{expected}), Arrays.toString(new List[]{actual}),
                "Student lists should be the same");
    }

    @Test
    @Tag("GET")
    @Order(2)
    @DisplayName("Get All Students Having Same Enrollment Year")
    void getStudentsByEnrollmentYear(){
        when(myStudents.getAllStudentsInEnrollmentYear(any(studentModel.class))).thenReturn(
                new LinkedList<>(Arrays.asList(
                        new studentModel("1152003", "Amr Khaled", "2015", "CCE-C"),
                        new studentModel("1152020", "Hussain Yahya", "2015", "CCE-C"),
                        new studentModel("1152021", "Mohamed Kadry", "2015", "CCE-E"),
                        new studentModel("1152022", "Ahmed Hatem", "2016", "CCE-C"),
                        new studentModel("1152023", "Ahmed Anwar", "2016", "CCE-E")
                )));

        myStudents.getAllStudentsInEnrollmentYear(
                new studentModel("1152003", "Amr Khaled", "2015", "CCE-C")
        );

        verify(myStudents, times(1)).getAllStudentsInEnrollmentYear(
          any(studentModel.class)
        );

    }


    @Tag("DELETE")
    @Nested
    @DisplayName("Delete a student by ID")
    class DeleteStudent{

        /*
        I know that nesting is not required here but it is just to try the concept
         */
        @Test
        @Order(3)
        @DisplayName("Mock student deleted")
        void studentWithIDDoesNotExist(){

            when(myStudents.deleteStudentWithID(any(studentModel.class))).thenReturn(Response
                    .status(Response.Status.OK)
                    .entity("Mock worked")
                    .build());

            myStudents.deleteStudentWithID(
                    new studentModel("1152003", "Amr Khaled", "2015", "CCE-C")
            );

            verify(myStudents, times(1)).deleteStudentWithID(
                    any(studentModel.class)
            );

        }
    }


    @Test
    @Tag("DELETE")
    @Order(5)
    @DisplayName("Spy Deleting a student that exists")
    void studentExistsDeletion(){
        myStudents = Mockito.spy(studentAPIs.class);
        System.out.println("Before: " + myStudents.getAllStudents());
        myStudents.deleteStudentWithID(
                new studentModel("1152003", "Amr Khaled", "2015", "CCE-C")
        );

        Assertions.assertEquals (myStudents.getAllStudents().size(), 4);
        System.out.println("After: " + myStudents.getAllStudents());
    }


    @Test
    @Tag("DELETE")
    @Order(4)
    @DisplayName("Spy Deleting a student that does not exist")
    void studentDoesNotExistDeletion(){
        myStudents = Mockito.spy(studentAPIs.class);
        System.out.println("Before: " + myStudents.getAllStudents());
        myStudents.deleteStudentWithID(
                new studentModel("1", "Amr Khaled", "2015", "CCE-C")
        );

        Assertions.assertEquals (myStudents.getAllStudents().size(), 5);
        System.out.println("After: " + myStudents.getAllStudents());
    }




}