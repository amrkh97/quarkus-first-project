package org.acme.resteasy.APIs;

import org.acme.resteasy.database.ClassDatabase;
import org.acme.resteasy.database.StudentsDatabase;
import org.acme.resteasy.models.classModel;
import org.acme.resteasy.models.responseModel;
import org.acme.resteasy.models.studentModel;

import javax.print.attribute.standard.Media;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/students")
public class studentAPIs {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String enteringStudentPage() {
        return "Welcome To The Students Page!";
    }

    @Path("/all")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<studentModel> getAllStudents() {
        return StudentsDatabase.getStudents();
    }


    @Path("all/{start}/{size}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<studentModel> paginateStudents(@PathParam("start") Integer start,
                                            @PathParam("size") Integer size) {
        return StudentsDatabase.paginateStudents(start, size);
    }

    @Path("deleteByID")
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteStudentWithID(studentModel student) {

        responseModel customResponse = StudentsDatabase.deleteStudentByID(student.getId());
        return Response.status(customResponse.getCode()).entity(customResponse)
                .header("Access-Control-Allow-Origin", "*").build();

    }

    @Path("add")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addNewStudent(studentModel studentModel) {
        responseModel customResponse = StudentsDatabase.addStudent(studentModel);
        return Response.status(customResponse.getCode()).entity(customResponse)
                .header("Access-Control-Allow-Origin", "*").build();

    }


    @Path("edit/{id}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editStudentData(@PathParam("id") String id, studentModel studentModel){

        responseModel customResponse = StudentsDatabase.editStudent(id, studentModel);
        return Response.status(customResponse.getCode()).entity(customResponse)
                .header("Access-Control-Allow-Origin", "*").build();
    }

    @Path("/year")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<studentModel> getAllStudentsInEnrollmentYear(studentModel studentModel) {
        return StudentsDatabase.getStudentsByEnrollmentYear(studentModel);
    }

}
