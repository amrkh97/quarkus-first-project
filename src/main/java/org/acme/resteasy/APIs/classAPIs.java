package org.acme.resteasy.APIs;

import org.acme.resteasy.database.ClassDatabase;
import org.acme.resteasy.database.StudentsDatabase;
import org.acme.resteasy.models.classModel;
import org.acme.resteasy.models.responseModel;
import org.acme.resteasy.models.studentModel;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/classes")
public class classAPIs {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String enteringClassesPage() {
        return "Welcome To The Students Page!";
    }

    @Path("all")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<classModel> getAllClasses() {
        return ClassDatabase.getClasses();
    }

    @Path("all/{start}/{size}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<classModel> paginateClasses(@PathParam("start") Integer start,
                                            @PathParam("size") Integer size) {
        return ClassDatabase.paginateClasses(start, size);
    }

    @Path("deleteByID")
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCourseWithID(classModel student) {

        responseModel customResponse = ClassDatabase.deleteCourseByID(student.getId());
        return Response.status(customResponse.getCode()).entity(customResponse)
                .header("Access-Control-Allow-Origin", "*").build();

    }

    @Path("add")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addNewClass(classModel classModel) {

        responseModel customResponse = ClassDatabase.addClass(classModel);
        return Response.status(customResponse.getCode()).entity(customResponse)
                .header("Access-Control-Allow-Origin", "*").build();

    }

}
