package org.acme.resteasy;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;


@Provider
@PreMatching
@Priority(Priorities.AUTHENTICATION)
public class RequestInterceptor implements ContainerRequestFilter {


    @Override
    public void filter(ContainerRequestContext requestContext){
        System.out.println("I'm inside the request interceptor");

        System.out.println("URI PATH: "+ requestContext.getUriInfo().getAbsolutePath());
        System.out.println("REQUEST METHOD: " + requestContext.getRequest().getMethod());

        System.out.println("Headers: ");
        System.out.println(requestContext.getHeaders());
    }



}


