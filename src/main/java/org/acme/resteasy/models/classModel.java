package org.acme.resteasy.models;

import java.util.List;

public class classModel {
    private  String id;
    private String name;
    private List<studentModel> classStudents;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<studentModel> getClassStudents() {
        return classStudents;
    }

    public classModel(String id, String name, List<studentModel> classStudents) {
        this.id = id;
        this.name = name;
        this.classStudents = classStudents;
    }

    public void setClassStudents(List<studentModel> classStudents) {
        this.classStudents = classStudents;
    }
}
