package org.acme.resteasy.models;

public class studentModel {
    private String id;
    private String name;
    private String enrollmentYear;
    private String Major;

    public studentModel(String id, String name, String enrollmentYear, String major) {
        this.id = id;
        this.name = name;
        this.enrollmentYear = enrollmentYear;
        Major = major;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEnrollmentYear() {
        return enrollmentYear;
    }

    public void setEnrollmentYear(String enrollmentYear) {
        this.enrollmentYear = enrollmentYear;
    }

    public String getMajor() {
        return Major;
    }

    public void setMajor(String major) {
        Major = major;
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", enrollmentYear='" + enrollmentYear + '\'' +
                ", Major='" + Major + '\'' +
                '}';
    }
}
