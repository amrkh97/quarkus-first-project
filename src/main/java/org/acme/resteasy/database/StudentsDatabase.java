package org.acme.resteasy.database;

import org.acme.resteasy.models.responseModel;
import org.acme.resteasy.models.studentModel;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class StudentsDatabase {
    private static List<studentModel> students = new LinkedList<studentModel>(Arrays.asList(

            new studentModel("1152003", "Amr Khaled", "2015", "CCE-C"),
            new studentModel("1152020", "Hussain Yahya", "2015", "CCE-C"),
            new studentModel("1152021", "Mohamed Kadry", "2015", "CCE-E"),
            new studentModel("1152022", "Ahmed Hatem", "2016", "CCE-C"),
            new studentModel("1152023", "Ahmed Anwar", "2016", "CCE-E")

    )
    );

    public static List<studentModel> getStudents() {
        return students;
    }

    public static responseModel addStudent(studentModel newStudent) {

        if (students.stream().anyMatch(ti -> ti.getId().equals(newStudent.getId()))) {
            return new responseModel(400, "Student with the same ID already exists!");
        } else {
            students.add(newStudent);
            return new responseModel(200, "Student added successfully!");
        }

    }

    public static responseModel deleteStudentByID(String id) {
        if (students.stream().anyMatch(ti -> ti.getId().equals(id))) {
            students.removeIf(obj -> obj.getId().equals(id));
            return new responseModel(200, "Student deleted from the list");
        } else {
            return new responseModel(400, "A Student having this id doesn't exist");
        }
    }

    public static responseModel editStudent(String id, studentModel studentModel) {

        if (students.stream().anyMatch(ti -> ti.getId().equals(id))) {

            studentModel oldStudentData = StudentsDatabase.getStudents().stream()
                    .filter(p -> p.getId().equals((id)))
                    .collect(Collectors.toList()).get(0);

            if (students.stream().anyMatch(ti -> ti.getId().equals(studentModel.getId()))
                    && !studentModel.getId().equals(id)) {

                return new responseModel(400, "Student with id : "
                        + studentModel.getId() + " already exists!");
            }

            students.set(students.indexOf(oldStudentData), studentModel);

            return new responseModel(200, "Student data edited");
        } else {
            return new responseModel(400, "A Student having this id doesn't exist");
        }

    }

    public static List<studentModel> getStudentsByEnrollmentYear(studentModel studentModel) {
        return StudentsDatabase.getStudents().stream()
                .filter(p -> p.getEnrollmentYear()
                        .equals((studentModel.getEnrollmentYear()))).collect(Collectors.toList());
    }

    public static List<studentModel> paginateStudents(Integer startFromIndex, Integer numberOfClasses) {
        if(students.size() < startFromIndex + numberOfClasses || students.size() < startFromIndex
                || startFromIndex < 0 || numberOfClasses < 0)
            return students;
        return students.subList(startFromIndex, startFromIndex+ numberOfClasses);
    }
}
