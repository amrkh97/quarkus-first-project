package org.acme.resteasy.database;

import org.acme.resteasy.models.classModel;
import org.acme.resteasy.models.responseModel;
import org.acme.resteasy.models.studentModel;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;

public class ClassDatabase {

    static Map<String, List<studentModel>> allStudents = StudentsDatabase.getStudents().stream()
            .collect(groupingBy(studentModel::getMajor));
    static List<studentModel> classComputer = new LinkedList<studentModel>(allStudents.get("CCE-C"));
    static List<studentModel> classElectronics = new LinkedList<studentModel>(allStudents.get("CCE-E"));

    private static List<classModel> classes = new LinkedList<classModel>(Arrays.asList(
            new classModel("CMPN-400","Computer Graphics",classComputer),
            new classModel("ELCN-201","Electronics",classElectronics)
            )
    );

    public static responseModel deleteCourseByID(String id) {
        if (classes.stream().anyMatch(ti -> ti.getId().equals(id))) {
            classes.removeIf(obj -> obj.getId().equals(id));
            return new responseModel(200, "Class "+ id +" was deleted from the list");
        } else {
            return new responseModel(400, "Class " + id +" does not exist!");
        }
    }

    public static responseModel addClass(classModel classModel) {

        if (classes.stream().anyMatch(ti -> ti.getId().equals(classModel.getId()))) {
            return new responseModel(400, "Class "+ classModel.getId()  +" already exists!");
        }

        if(classModel.getId().contains("CMPN") || classModel.getId().contains("ELCN")){
            List<studentModel> newClassStudents;

            if(classModel.getId().contains("CMPN")){
                newClassStudents = classComputer;
            }else{
                newClassStudents = classElectronics;
            }

            classModel newClass = new classModel(classModel.getId(),classModel.getName(),newClassStudents);
            classes.add(newClass);
            return new responseModel(200, "Student added successfully!");

        }else{
            return new responseModel(400, "Class name is invalid! it should contain only CMPN or ELCN");
        }
    }

    public List<studentModel> getClassComputer() {
        return classComputer;
    }

    public List<studentModel> getClassElectronics() {
        return classElectronics;
    }

    public static List<classModel> paginateClasses(int startFromIndex, int numberOfClasses){
        if(classes.size() < startFromIndex + numberOfClasses || classes.size() < startFromIndex
        || startFromIndex < 0 || numberOfClasses < 0)
            return classes;
        return classes.subList(startFromIndex, startFromIndex+ numberOfClasses);
    }

    public static List<classModel> getClasses() {
        return classes;
    }
}
