package org.acme.resteasy;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


@ApplicationPath("/api")
public class MainApp extends Application {

}
